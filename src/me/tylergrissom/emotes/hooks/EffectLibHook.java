package me.tylergrissom.emotes.hooks;

import de.slikey.effectlib.EffectManager;
import org.bukkit.Bukkit;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class EffectLibHook {

    private JavaPlugin plugin;
    private EffectManager effectManager;

    public JavaPlugin getPlugin() {
        return plugin;
    }

    public EffectManager getEffectManager() {
        return effectManager;
    }

    public boolean isHooked() {
        return effectManager != null;
    }

    public EffectLibHook(JavaPlugin plugin) {
        this.plugin = plugin;

        if (Bukkit.getPluginManager().getPlugin("EffectLib") == null) {
            Bukkit.getLogger().warning("EffectLib not found. Disabling configuration-based emote particle effects.");

            this.effectManager = null;

            return;
        }

        this.effectManager = new EffectManager(plugin);

        Bukkit.getLogger().info("EffectLib found. Enabling configuration-based emote particle effects.");
    }
}
