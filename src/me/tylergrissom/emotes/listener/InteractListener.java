package me.tylergrissom.emotes.listener;

import me.tylergrissom.emotes.EmotesPlugin;
import me.tylergrissom.emotes.inventory.EmoteSelectInventory;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerInteractEntityEvent;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class InteractListener implements Listener {

    private EmotesPlugin plugin;

    public InteractListener(EmotesPlugin plugin) {
        this.plugin = plugin;
    }

    @EventHandler
    public void onEntityInteractWithEntity(final PlayerInteractEntityEvent event) {
        final Player player = event.getPlayer();

        if (event.getRightClicked() instanceof Player) {
            Player clicked = ((Player) event.getRightClicked());
            EmoteSelectInventory emoteSelectInventory = new EmoteSelectInventory(plugin, player, clicked);

            emoteSelectInventory.open();
        }
    }
}
