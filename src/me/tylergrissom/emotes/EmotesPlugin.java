package me.tylergrissom.emotes;

import me.tylergrissom.emotes.api.EmoteApi;
import me.tylergrissom.emotes.hooks.EffectLibHook;
import org.bukkit.event.HandlerList;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class EmotesPlugin extends JavaPlugin {

    private EmotesPlugin plugin;
    private EffectLibHook effectLibHook;
    private EmoteApi emoteApi;

    public EmotesPlugin getPlugin() {
        return plugin;
    }

    public EffectLibHook getEffectLibHook() {
        return effectLibHook;
    }

    public EmoteApi getEmoteApi() { return emoteApi; }

    @Override
    public void onEnable() {
        plugin = this;
        effectLibHook = new EffectLibHook(this);
        emoteApi = new EmoteApi(this);
    }

    @Override
    public void onDisable() {
        HandlerList.unregisterAll(this);
    }
}
