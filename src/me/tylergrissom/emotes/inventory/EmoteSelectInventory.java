package me.tylergrissom.emotes.inventory;

import me.tylergrissom.emotes.EmotesPlugin;
import me.tylergrissom.emotes.api.EmoteApi;
import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;

import java.util.HashSet;
import java.util.Set;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class EmoteSelectInventory {

    private EmotesPlugin plugin;
    private Player player, clicked;

    public EmoteSelectInventory(EmotesPlugin plugin, Player player, Player clicked) {
        this.plugin = plugin;
        this.player = player;
        this.clicked = clicked;
    }

    public Inventory getInventory() {
        int size = 9;

        // TODO add logic for auto-resizing inventory

        Inventory inventory = Bukkit.createInventory(null, size, "Emotes");
        Set<EmoteApi.EmoteData> emoteDataSet = plugin.getEmoteApi().getAllEmoteData();

        

        return inventory;
    }

    public void open() {
        player.playSound(player.getLocation(), Sound.UI_BUTTON_CLICK, 1F, 1F);
        player.openInventory(getInventory());
    }
}
