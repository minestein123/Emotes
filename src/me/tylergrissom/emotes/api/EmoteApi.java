package me.tylergrissom.emotes.api;

import me.tylergrissom.emotes.EmotesPlugin;
import org.bukkit.configuration.ConfigurationSection;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Copyright (c) 2013-2016 Tyler Grissom
 */
public class EmoteApi {

    public class EmoteData {

        private String name, displayName, senderMessage, receiverMessage, broadcast;
        private boolean enabled, valid;
        private ConfigurationSection configurationSection;

        public String getName() {
            return this.name;
        }

        public String getDisplayName() {
            return this.displayName;
        }

        public String getSenderMessage() {
            return this.senderMessage;
        }

        public String getReceiverMessage() {
            return this.receiverMessage;
        }

        public String getBroadcast() {
            return this.broadcast;
        }

        public boolean getEnabled() {
            return this.enabled;
        }

        public ConfigurationSection getConfigurationSection() {
            return configurationSection;
        }

        public EmoteData(String name) {
            this.name = name;

            if (emotesPlugin.getConfig().getConfigurationSection("emotes." + name) == null) {
                valid = false;

                return;
            }

            this.configurationSection = emotesPlugin.getConfig().getConfigurationSection("emotes." + name);
            this.displayName = this.configurationSection.getString("display-name", name + ": Display Name Unset");
            this.enabled = this.configurationSection.getBoolean("enabled", true);
        }

        public boolean isValid() {
            return this.valid;
        }
    }

    private EmotesPlugin emotesPlugin;

    /**
     * Returns an the instance of EmotesPlugin
     * @return The instance of EmotesPlugin
     */
    public EmotesPlugin getEmotesPlugin() {
        return this.emotesPlugin;
    }

    /**
     * Instantiates a new EmoteAPI (don't use this, access through EmotesPlugin class)
     * @param emotesPlugin The instance of EmotesPlugin
     */
    public EmoteApi(EmotesPlugin emotesPlugin) {
        this.emotesPlugin = emotesPlugin;
    }

    /**
     * Checks if <code>emote</code> exists
     * @param emote The emote to check for
     * @return Whether or not the emote exists
     */
    public boolean exists(String emote) {
        return emotesPlugin.getConfig().get("emotes." + emote) != null;
    }

    /**
     * Gets EmoteData for any emote named under <code>emote</code>
     * <p>
     *     Note: Essentially the equivalent of instantiating a new EmoteData.
     * </p>
     * @param emote The emote to find
     * @return The new instance of EmoteData
     */
    public EmoteData getEmoteData(String emote) {
        return new EmoteData(emote);
    }

    /**
     * Checks if <code>emote</code> is valid
     * @param emote The emote to check for
     * @return Whether or not the emote is valid
     */
    public boolean isValid(String emote) {
        return exists(emote) && getEmoteData(emote).isValid();
    }

    /**
     * Gets all emote names
     * @return  The emote names in a list format
     */
    public List<String> getAllEmoteNames() {
        List<String> list = new ArrayList<>();

        list.addAll(emotesPlugin.getConfig().getConfigurationSection("emotes").getKeys(false));

        return list;
    }

    /**
     * Gets all EmoteData
     * @return The EmoteData for every valid emote in the configuration
     */
    public Set<EmoteData> getAllEmoteData() {
        Set<EmoteData> set = new HashSet<>();

        for (String string : getAllEmoteNames()) {
            EmoteData emoteData = getEmoteData(string);

            if (emoteData.isValid()) set.add(emoteData);
        }

        return set;
    }
}
