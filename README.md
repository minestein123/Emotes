Emotes 1.9
==========

A configuration based Emotes plugin for Bukkit and Spigot 1.9+

TODO
----

* Implement configuration-based particle effects with EffectLib
* Deeper EmoteApi integration with more methods
* Publish documentation for EmoteApi
* Set up Wiki on GitLab